package org.woehlke.simulation.evolution;

public interface SimulatedEvolution {

    String SPACE = "      ";

    String SUB_TITLE = SPACE
        + "Artificial Life Simulation of Bacteria Motion depending on DNA";

    String TITLE = "Simulated Evolution";

    String FOOTER = SPACE + "(C) 2018 Thomas Woehlke";

    int HEIGHT_OF_TITLE = 30;

    int START_POSITION_ON_SCREEN_X = 100;

    int START_POSITION_ON_SCREEN_Y = 100;

    int SCALE = 2;

    int WIDTH = 320;

    int HEIGHT = 234;

    int EXIT_STATUS = 0;
}
